# README #

Project for printing source files to the docx file.

### Usefull links ###

[OpenXML SDK](http://www.microsoft.com/en-us/download/details.aspx?id=5124)

[Example](http://stackoverflow.com/questions/19656626/how-do-i-create-the-docx-document-with-microsoft-office-interop-word)

### How do I get set up? ###

* Download and install OpenXML SDK (link above)
* Add reference to the DocumentFormat.OpenXml assembly:  
Add Reference -> Assemblies -> Extensions -> DocumentFormat.OpenXml (2.5)
* Add reference to the WindowsBase:
Add Reference -> Assemblies -> Framework -> WindowsBase (4.0.0.0)