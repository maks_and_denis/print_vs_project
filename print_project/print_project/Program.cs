﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;


namespace print_project
{
    class Program
    {
        static void Main(string[] args)
        {
            // Name of the input text file with code.
            string inputFileName = "output_2.txt";

            // Name of the output docx document.
            string outputFileName = "test.docx";

            List<string> data = new List<string>();


            #region Read the input file.
            try
            {
                // Read the input file and store data in the list.
                using (StreamReader sr = new StreamReader(inputFileName))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        data.Add(line);

                        //Console.WriteLine(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Can't read the input file:");
                Console.WriteLine(e.Message);
            } 
            #endregion
            

            #region Create docx document.
            using (var document = WordprocessingDocument.Create(
        outputFileName, WordprocessingDocumentType.Document))
            {
                MainDocumentPart mainPart = document.AddMainDocumentPart();

                mainPart.Document = new Document();

                var body = new Body();
                var paragraph = new Paragraph();
                var run = new Run();


                // Loop through the list, create Text and add to the Run.
                foreach (var line in data)
                {
                    var text = new Text()
                    {
                        Text = line,
                        Space = SpaceProcessingModeValues.Preserve
                    };

                    // Add text.
                    run.AppendChild<Text>(text);

                    // Add CarriageReturn.
                    run.AppendChild<CarriageReturn>(new CarriageReturn());
                }


                paragraph.AppendChild<Run>(run);
                body.AppendChild<Paragraph>(paragraph);
                mainPart.Document.AppendChild<Body>(body);


                #region Define ParagraphProperties.
                // Create paragraph properties.
                ParagraphProperties paragraphProperties = new ParagraphProperties();

                // Set the spacing between lines.
                SpacingBetweenLines spacing = new SpacingBetweenLines()
                {
                    Line = "0",
                    LineRule = LineSpacingRuleValues.Exact,
                    Before = "0",
                    After = "0"
                };

                paragraphProperties.AppendChild<SpacingBetweenLines>(spacing);

                // Apply ParagraphProperties.
                paragraph.PrependChild<ParagraphProperties>(paragraphProperties);
                #endregion


                #region Define RunProperties.
                // Create run properties.
                RunProperties runProperties = new RunProperties();

                // Set the font.
                RunFonts runFont = new RunFonts
                {
                    Ascii = "Courier New"
                };

                // Set the font size.
                FontSize size = new FontSize
                {
                    Val = new StringValue("20")
                };

                runProperties.AppendChild<RunFonts>(runFont);
                runProperties.AppendChild<FontSize>(size);

                // Apply RunProperties.
                run.PrependChild<RunProperties>(runProperties);
                #endregion

                // Save changes to the MainDocumentPart part.
                mainPart.Document.Save();
            } 
            #endregion


            //Console.ReadKey();
        }
    }
}
